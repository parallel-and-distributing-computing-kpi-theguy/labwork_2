Laboratory work #2 for Parallel and Distributing Computing.

Purpose for the work: To study and practice how to create threads in programming language - Java.
Programming language: Java
Used technologies: In Java multithreading works as extending the class Thread, or implementing the interface Runnable. All code, that is written inside function run() will work in parallel threads of CPU, when the method start() is called through the object, that implements Thread/Runnable.

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.
Program works for 3 threads, all returns the result.