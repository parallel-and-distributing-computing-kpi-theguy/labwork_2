/*------------------PaDC-----------------------------
---------------Labwork #2----------------------------
------------Threads in Java--------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--30.09.2017-----------------------------------------*/

package com.kpi;

public class Data {

    private int N;
    public Data (int N){
        this.N = N;
    }
    public void Matrix_Input (int[][] matrix){
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                matrix[i][j] = 1;
            }
        }
    }
    public void Matrix_Output (int[][] matrix){
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                System.out.print(matrix[i][j]+"          ");
            }
            System.out.println();
        }
        System.out.println();
    }
    public void Vector_Input (int[] vector){
        for (int i = 0; i < N; i++){
            vector[i] = 1;
        }
    }
    public void Vector_Output (int[] vector){
        for (int i = 0; i < N; i++){
            System.out.print(vector[i]+"      ");
        }
        System.out.println();
    }


    private int[][] Multiply_Matrixes (int[][] MA, int[][] MB){
        int[][] result = new int[N][N];
        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                int cell = 0;
                for (int k = 0; k < N; k++){
                    cell += MA[i][k] * MB[k][j];
                }
                result[i][j] = cell;
            }
        }
        return result;
    }
    private int[] Multiply_Matr_Vect (int[][] MA, int[] A){
        int[] result = new int[N];
        for(int i = 0; i < N; i++){
            int cell = 0;
            for(int j = 0; j < N; j++){
                cell += A[i] * MA[j][i];
            }
            result[i] = cell;
        }
        return result;
    }
    private int Multiply_Vectors (int[] A, int[] B){
        int result = 0;
        for(int i = 0; i < N; i++){
            result = 0;
            for (int j = 0; j < N; j++){
                result += A[i] * B[j];
            }
        }
        return result;
    }
    private int[][] Sum_Matrix (int[][] MA, int[][] MB){
        int[][] result = new int[N][N];
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                result[i][j] = MA[i][j] + MB[i][j];
            }
        }
        return result;
    }
    private int[] Sum_Vectors (int[] A, int[] B){
        int[] result = new int[N];
        for(int i = 0; i < N; i++){
            result[i] = A[i] + B[i];
        }
        return result;
    }
    private void Transpose_Matrix (int[][] MA){
        int cell;
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                cell = MA[j][i];
                MA[j][i] = MA [i][j];
                MA [i][j] = cell;
            }
        }
    }
    private void Sort_Vector (int[] A){
        for (int i = 0; i < N - 1; i++){
            int min = i;
            for (int j = i + 1; j < N; j++){
                if (A[min] > A[j])
                    min = j;
            }
            int cell = A[i];
            A[i] = A[min];
            A[min] = cell;
        }
    }
    //Function 1 d = ((A*B) + (C*(B*(MA*MD))))
    public int F1 (int[] A, int[] B, int[] C, int[][] MA, int[][] MD){
        int x = Multiply_Vectors(A,B);
        int[][] MK = Multiply_Matrixes(MA,MD);
        int[] K = Multiply_Vect_Int(B,x);
        int[] y = Multiply_Vectors(C,K);
        return (x + y);
    }
    //Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)
    public int[][] F2 (int[][] MA, int[][] MB, int[][] MM, int[][] MX){
        int[][] ME = MA;
        Transpose_Matrix(ME);
        int[][] MF = Multiply_Matrixes(MB,MM);
		Transpose_Matrix(MF)
        int[][] MR = Multiply_Matrixes(ME,MF);
        ME = Sum_Matrix(MR,MX);
        return ME;
    }
    //Function 3 T = (SORT(O + P) * TRANS(MR * MS)
    public int[] F3 (int[] O, int[] P, int[][] MR, int[][] MS){
        int[] A = Sum_Vectors(O,P);
        Sort_Vector(A);
        int[][] MB = Multiply_Matrixes(MR,MS);
		Transpose_Matrix(MB);
        int[] B = Multiply_Matr_Vect(MB,A);
        return B;
    }
}
