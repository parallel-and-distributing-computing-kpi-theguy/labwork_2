/*------------------PaDC-----------------------------
---------------Labwork #2----------------------------
------------Threads in Java--------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--30.09.2017-----------------------------------------*/

package com.kpi;
public class Func2 implements Runnable {
    private int N;

    public Func2(int n) {
        this.N = n;
    }

    @Override
    public void run() {
        try {
            Data data = new Data(N);
            System.out.println("Task #2 started");
            Thread.sleep(2000);
            int[][] MA = new int[N][N];
            int[][] MB = new int[N][N];
			int[][] MM = new int[N][N];
            int[][] MX = new int[N][N];
            data.Matrix_Input(MA);
            data.Matrix_Input(MB);
            data.Matrix_Input(MM);
			data.Matrix_Input(MX);
            int[][] MK = data.F2(MA, MB, MM, MX);
            Thread.sleep(2000);
            if (N < 10) {
                System.out.println("T2: MK = ");
                data.Matrix_Output(MK);
            }
            System.out.println("Task #2 finished");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
