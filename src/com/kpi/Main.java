/*------------------PaDC-----------------------------
---------------Labwork #2----------------------------
------------Threads in Java--------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--30.09.2017-----------------------------------------*/


package com.kpi;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter N:");
        int N = scan.nextInt();
        Func1 T1 = new Func1(N);
        T1.setPriority(Thread.MIN_PRIORITY);
        Func2 F2 = new Func2(N);
        Thread T2 = new Thread(F2, "T2");
        T2.setPriority(Thread.MAX_PRIORITY);
        Func3 T3 = new Func3(N);
        T3.setPriority(Thread.NORM_PRIORITY);
        T1.start();
        T2.start();
        T3.start();
        T1.join();
        T2.join();
        T3.join();
        System.out.println("Main class finished");
    }
}
