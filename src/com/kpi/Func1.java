/*------------------PaDC-----------------------------
---------------Labwork #2----------------------------
------------Threads in Java--------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--30.09.2017-----------------------------------------*/

package com.kpi;
public class Func1 extends Thread {
    private int N;

    public Func1(int n) {
        this.N = n;
    }

    @Override
    public void run() {
        try {
            setName("T1");
            Data data = new Data(N);
            System.out.println("Task #1 started");
            sleep(2000);
			int[] A = new int[N];
            int[] B = new int[N];
            int[] C = new int[N];
            int[][] MA = new int[N][N];
            int[][] MD = new int[N][N];
			data.Vector_Input(A);
            data.Vector_Input(B);
            data.Vector_Input(C);
            data.Matrix_Input(MA);
            data.Matrix_Input(MD);
            int d = data.F1(A, B, C, MA, MD);
            sleep(1000);
            if (N < 10) {
                System.out.println("T1: d = " + d);
            }
            System.out.println("Task #1 finished");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
