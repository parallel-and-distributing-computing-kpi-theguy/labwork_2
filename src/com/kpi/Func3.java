/*------------------PaDC-----------------------------
---------------Labwork #2----------------------------
------------Threads in Java--------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--30.09.2017-----------------------------------------*/

package com.kpi;
public class Func3 extends Thread {
    private int N;

    public Func3(int n) {
        this.N = n;
    }

    @Override
    public void run() {
        try {
            Data data = new Data(N);
            setName("T3");
            System.out.println("Task #3 started");
            sleep(2000);
            int[] O = new int[N];
            int[] P = new int[N];
            int[][] MR = new int[N][N];
            int[][] MS = new int[N][N];
            data.Vector_Input(O);
            data.Vector_Input(P);
            data.Matrix_Input(MR);
            data.Matrix_Input(MS);
            int[] T = data.F3(O, P, MR, MS);
            sleep(3000);
            if (N < 10) {
                System.out.println("T3: T = ");
                data.Vector_Output(T);
            }
            System.out.println("Task #3 finished");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
